# Live ISO for schools

This repositories contains instruction and files required to create Live ISO required by ILUGBOM to install linux in schools.
The resulting ISO will be based on debian and will have a slightly customised xfce desktop enviroment.

We are not rebranding or customising anything (apart rom some defaults that the teacher prefer) so this is pure debian with some packages bundled in. Please do not miss-understand this as an effort to build custom distro as this is nothing but debian.

The main motive for building this iso is saving time while installing linux on more than 30 computers on avarage, this Iso takes roughly 10-20 minutes and no intervention apart from selecting hdd and grub options, helpful when your volunteers are new to linux too. These computers often don't have Lan or network so installing over lan is not an option. 

Apart from some common system packages the packages pre-installed in the iso are given here:

* chromium
* cups
* firefox-esr
* geogebra
* gimp
* gnome-orca
* inkscape
* kalzium
* kdenlive
* kolourpaint4
* ktouch
* libreoffice
* libreoffice-gtk
* network-manager
* pidgin
* python-pyatspi2
* python3-pyatspi2
* simple-scan
* gnome-mahajong
* gnome-mines
* aisleriot
* scratch
* step
* synaptic
* tuxmath
* tuxpaint
* thunderbird
* synfigstudio
* scribus
* vlc
* grub-pc
* ucblogo
* kturtle
* xfce4
* xfce4-goodies
* guvcview
* bluefish

The above packages are based on demands from teachers, we add or remove packages according to the needs of particular school.

# Instruction to build your iso (you will need debian 9 stretch to build and iso)

* clone this repository

```
git clone https://gitlab.com/raghukamath/live-iso-for-schools.git liveiso
```

* Then install the required packages

```bash
apt update && apt install live-build squashfs-tools git
cd liveiso/live-default
lb config --archive-areas "main contrib non-free" --debian-installer live

```
* change the default root and student password preseed file 

open the pressed.cfg file in /live-default/config/includes.installer/
and go to line 28 or search for root-password and student-password change this text to a password that you want to set for root and student user

* change to root user with su command and then build

```
lb build 2>&1 | tee build.log
```
Note: the build.log file will have is the output of the build process, in case you get errors, it will be helpful to inspect this file
It will take some time to download and build the iso.
After the process you will have the iso in the live-default folder

**For more detailed instructions read the live-iso.md file**

Any help in improving this process or files in this repository is welcome. We need help from experts to perfect this setup.
