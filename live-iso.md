*Notes: $ means normal user mode # means root mode  ## are comments*

# setting up a VM with debian 9
*If you have debian 9 as your OS then skip this step you can directly start building ISO*
We need to first setup a vm to build our ISO

1. Enable virtualisation technology in your bios
2. Install qemu, bridge-utils from your distributions repository
3. Create a local folder to place the vm hard disk images  

        $ mkdir vm-files # create new folder
        $ cd vm-files # change directory
        $ qemu-img create -f raw debian-test.raw 40G  # this will create a new 40gb hard disk image named debian-test in current directory.

4. In order to get network access inside the vm we have to create a network bridge

        ## you can save this code snippet in a .sh file and run it everytime when you boot to vm 
        ## this will need root access so change user to root with 'su'
        
        # brctl addbr br0
        # ip addr add 192.168.2.1/24 broadcast 192.168.2.255 dev br0
        # ip tuntap add dev tap0 mode tap
        # ip link set tap0 up promisc on
        # brctl addif br0 tap0
        # dnsmasq --interface=br0 --bind-interfaces --dhcp-range=192.168.2.10,192.168.2.254
        # iptables -I FORWARD -m physdev --physdev-is-bridged -j ACCEPT


5. Now download the debian 9 dvd from debian website, to save time directly download it with a torrent from [here](https://cdimage.debian.org/debian-cd/current/amd64/bt-dvd/).

6. Now we will install Debian in the VM. We need to boot the hard disk image with the debian iso image

        $ qemu-system-x86_64 -drive file=path/to/disk/image/debian-test.raw,format=raw -m 6G -net nic -net user -enable-kvm -cpu host -vga qxl -cdrom /path/to/ISO/debian-9.1.0-amd64-DVD-1.iso -boot d
        ## boot -d will ensure that we are booting from the ISO and not the hdd image.
        ## -m is the memmory you provide to the vm so choose accordingly. Here I have kept it 6GB

7. Choose Graphical Install and follow the instruction of the installer. They are pretty straight forward. After you have done with installer, click * Machine > Power down * from qemu window to shutdown otherwise we will reboot into the ISO

8. Start the VM now

        $ qemu-system-x86_64 -drive file=path/to/disk/image/debian-test.raw,format=raw -m 6G -net nic -net user -enable-kvm -cpu host -vga qxl
        ## -m is the memmory you provide to the vm so choose accordingly. Here I have kept it 6GB
        

    
9. First step is done now the actual live iso build starts

...

# Building the Live ISO

1. update the source.lists file in debian, comment out everything and add the following

        deb  http://deb.debian.org/debian stretch main
        deb-src  http://deb.debian.org/debian stretch main

        deb  http://deb.debian.org/debian stretch-updates main
        deb-src  http://deb.debian.org/debian stretch-updates main

        deb http://security.debian.org/ stretch/updates main
        deb-src http://security.debian.org/ stretch/updates main
        
        ## by default debian installed from dvd has cdrom as repository we want the online repos so add the above lines, these lines are from [debian wiki](https://wiki.debian.org/SourcesList).

    
2. Install the pre-requisite packages

        # apt update && apt install live-build squashfs-tools git

3. create a work folder in your home directory

        $ mkdir live-default && cd live-default
        
4. Add configuration

        $ lb config --archive-areas "main contrib non-free" --debian-installer live
        
***Note more configurations and personalisations are necessary need to update this instruction file***
        
5. Now build *note you should be in root mode change with 'su'*

        # lb build

You will now have a .ISO file in the current directory


# Add packages to the live iso

when you run the lb config command a config folder will get created in the live-default directory
You will see a folder name package-lists inside it.

Find the package name of your software with the help of synaptic or apt search command and add the names to a text file, save it inside the package-lists folder as 'my.list.chroot'.
The name can be anything for example mysoftwares.list.chroot the .list.chroot should be present though.

# Use apt-cacher to download to cache incase you build repeatedly
check debian manual to setup apt-cacher ng
while lb config pass this additional option as shown below

        lb config --apt-http-proxy http://10.0.2.15:3142 --debian-installer live

# Add files to the User's directory

If you want to add files to users dirrectory or root folder.
Add them to the includes.chroot folder.
The will be the root of the installation, and you can add anything to the users home directory by creating a home > username folders
For example to put a .desktop file on the live users desktop folder we make a folder name home then a folder named user (username of your live user) and again a folder named Desktop and put the relevant file inside this folder. This file will copied to the resulting live users desktop folder.

# To change the bootloader splash image
change the files in live-default/config/bootloaders/ to customise the bootloader splash image
the splash image is stored in respective bootloader folder as splash.png or splash.svg, replace it with your own image

Note:
The preseed file will be disregarded if the user runs installer from the live enviroment. We have not included a link to install OS within live install. Installer with preseed works only from the bootmenu. If you run installer from live enviroment you will have to gothrough the default installation questions that debian has.

# References

* https://debian-live.alioth.debian.org/live-manual/stable/manual/html/live-manual.en.html
* https://git.hamaralinux.org/hamara-developers/hamara-live-build
